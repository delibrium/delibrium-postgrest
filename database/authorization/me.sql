create or replace function aula.me()
  returns json
  language plpython3u
as $$
import json

maybe_user_id = plpy.execute("select current_setting('request.jwt.claim.user_id', true)")[0]['current_setting']
if maybe_user_id:
  # Get user data
  get_user = plpy.prepare('select * from aula.users where id = $1', ["bigint"])
  usr = plpy.execute(get_user, [maybe_user_id])[0]

  # Get custom roles
  rv = plpy.execute('select r.name, array_agg(ur.idea_space) from aula.roles as r join aula.user_role as ur on r.id = ur.role_id where ur.user_id  = {} group by r.name'.format(maybe_user_id));

  roles = {}
  for c in rv:
    roles[c['name']] = list(c['array_agg'])

  usr['roles'] = roles

  # Get permissions
  rv = plpy.execute('select distinct r.name,r.permissions from aula.roles as r join aula.user_role as ur on r.id = ur.role_id where user_id = {}'.format(maybe_user_id));

  permissions = {}
  for p in rv:
    permissions[p['name']] = json.loads(p['permissions'])

  usr['permissions'] = permissions  

  return json.dumps(usr)
$$;
