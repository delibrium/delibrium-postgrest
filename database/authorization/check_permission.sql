create or replace function aula.check_permission(permission text, userid bigint, idea_space_id bigint, schoolid bigint)
  returns boolean
  language plpgsql
as $$
declare
  check_result_no_space boolean;
  check_result_space boolean;
  check_result_all boolean;
begin

  if idea_space_id is null then
    select count(*) > 0 into check_result_no_space from aula.roles as r join aula.user_role as ur on r.id = ur.role_id where r.permissions->cast(permission as text) ?| array['all', 'school', 'school_and_idea_space'] and ur.user_id = userid and ur.school_id = schoolid;
    return check_result_no_space;
  else
    select count(*) > 0 into check_result_space from aula.roles as r join aula.user_role as ur on r.id = ur.role_id where r.permissions->cast(permission as text) ?| array['idea_space', 'school_and_idea_space'] and ur.user_id = userid and ur.idea_space = idea_space_id and ur.school_id = schoolid;
    select count(*) > 0 into check_result_all from aula.roles as r join aula.user_role as ur on r.id = ur.role_id where r.permissions->cast(permission as text) ?| array['all'] and ur.user_id = userid and ur.school_id = schoolid;
    return (check_result_space is True or check_result_all is True);
 end if;
end
$$;

grant execute on function aula.check_permission (text, bigint, bigint, bigint) to aula_authenticator;
