create or replace function aula_secure.user_id(username text, password text)
  returns table (uid bigint, sc integer)
  language plpgsql
as $$
  begin
    set "request.jwt.claim.is_admin" TO 'True';

    return query
      select
        aula.users.id,
        aula_secure.user_login.session_count
      from
        aula.users,
        aula_secure.user_login
      where
        aula.users.user_login_id = aula_secure.user_login.id
        and aula.users.username = user_id.username
        and aula_secure.user_login.password = crypt(
          user_id.password,
          aula_secure.user_login.password
        );
  end;
$$;

grant execute on function aula_secure.user_id(text, text) to aula_authenticator;
