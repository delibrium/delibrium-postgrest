create or replace function aula.can_update_idea(idea aula.idea, has_edit_idea_permission boolean, has_edit_topic_permission boolean, has_mark_idea_winner_permission boolean)
  returns boolean
  language plpython3u
as $$
  # Get User id
  res_calling_user_id = plpy.execute(
    "select current_setting('request.jwt.claim.user_id')::int;"
  )
  if len(res_calling_user_id) == 0:
    plpy.error('Did not find user associated with this request.', sqlstate='PT401')
  calling_user_id = res_calling_user_id[0]['current_setting']

  # Get old idea
  get_old_idea = plpy.prepare("select * from aula.idea where id = $1;", ["bigint"])
  rv = plpy.execute(get_old_idea, [idea["id"]])

  # Get columns from old idea
  old_topic = rv[0]["topic"]
  new_topic = idea["topic"]
  old_selected = rv[0]["selected"]
  new_selected = idea["selected"]
  created_by = idea["created_by"]

  # Check if only allowed fields were changed
  if old_topic != new_topic:
    if has_edit_topic_permission:
      return True;

  if old_selected != new_selected:
    if has_mark_idea_winner_permission:
      return True;

  if old_topic == new_topic and old_selected == new_selected:
    if has_edit_idea_permission:
      return True;
    if created_by == calling_user_id:
      return True;

  plpy.error("Insufficent permission for updating idea.", sqlstate='PT401')
  return False;
$$;
