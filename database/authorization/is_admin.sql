create or replace function aula.is_admin(school_id bigint)
  returns boolean
  language plpgsql
as $$
begin
  if current_setting('app.debug') then
    raise info 'CHECK IS ADMIN';
    raise info 'is_admin => %, school_id => %', current_setting('request.jwt.claim.is_admin', true), current_setting('request.jwt.claim.school_id', true);
  end if;
  return (current_setting('request.jwt.claim.is_admin', true) = 'True');
end
$$;
