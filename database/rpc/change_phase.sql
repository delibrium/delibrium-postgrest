create or replace function aula.change_phase(topic bigint, phase text)
    returns json
    language plpython3u
as $$
  import json
  if phase not in ['edit_topics', 'feasibility', 'vote', 'finished']:
    return
  update_phase = plpy.prepare("""
    update aula.topic
    set
        phase='{phase}',
        config=jsonb_set(config, '{{{phase}_started}}', to_json(now())::jsonb, true)
    where id=$1
    returning config;
  """.format(phase=phase), ["bigint"])
  rv = plpy.execute(update_phase, [topic])
  return rv[0]['config'] if len(rv) > 0 else '{}'
$$;

grant execute on function aula.change_phase(bigint, text) to aula_authenticator;
