create or replace function aula.delete_space(school_id bigint, space_id bigint)
    returns json
    language plpython3u
as $$
    import json

    # Get current user id
    current_user_id = plpy.execute("select current_setting('request.jwt.claim.user_id', true)")[0]['current_setting']

    # Get school id from JWT
    res_school_id = plpy.execute("select current_setting('request.jwt.claim.school_id');")
    if len(res_school_id) == 0:
      plpy.error('Current user is not associated with a school.', sqlstate='PT401')
    school_id = res_school_id[0]['current_setting']

    # Check if user is admin
    is_admin_plan = plpy.prepare("select aula.is_admin($1);", ["bigint"])
    is_admin = plpy.execute(is_admin_plan, [school_id])
    
    # Check if user is school_admin
    is_school_admin_plan = plpy.prepare("select aula.is_school_admin($1);", ["bigint"])
    is_school_admin = plpy.execute(is_school_admin_plan, [school_id])

    # Check if user has permission to delete idea space
    check_delete_space_permission = plpy.prepare("select aula.check_permission('delete_room', $1, null, $2)", ["bigint", "bigint"])
    has_delete_space_permission = plpy.execute(check_delete_space_permission, [current_user_id, school_id])

    if is_admin[0]['is_admin'] or is_school_admin[0]['is_school_admin'] or has_delete_space_permission[0]['check_permission']:
      plpy.execute('set "request.jwt.claim.is_admin" TO "True"')
      #  delete comment
      ideas = plpy.execute('select id from aula.idea where school_id = {} and idea_space = {}'.format(school_id, space_id))
      for idea in ideas:
        # Delete comments
        comments = plpy.execute('select id from aula.comment where parent_idea = {}'.format(idea['id']))
        for comment in comments:
          plpy.execute('delete from aula.comment_vote where comment = {}'.format(comment['id']))
        plpy.execute('delete from aula.comment where parent_idea = {}'.format(idea['id']))
        # Delete idea votes
        plpy.execute('delete from aula.idea_vote where idea = {}'.format(idea['id']))
        # Delete idea like
        plpy.execute('delete from aula.idea_like where idea = {}'.format(idea['id']))
        # Delete idea feasibility
        plpy.execute('delete from aula.feasible where idea = {}'.format(idea['id']))
        # Delete idea
        plpy.execute('delete from aula.idea where id = {}'.format(idea['id']))

      # Delete assigned user roles
      roles = plpy.execute('delete from aula.user_role where idea_space = {} and school_id = {}'.format(space_id, school_id))
      # Delete topics
      topics = plpy.execute('select id from aula.topic where idea_space = {}'.format(space_id))
      for topic in topics:
        plpy.execute('delete from aula.delegation where context_topic = {}'.format(topic['id']))
      plpy.execute('delete from aula.topic where idea_space = {}'.format(space_id))
      # Delete Idea Space
      plpy.execute('delete from aula.idea_space where id = {}'.format(space_id))
    else:
      plpy.error('Insufficient rights to delete idea space.', not is_school_admin[0]['is_school_admin'])

    return json.dumps([])
$$;