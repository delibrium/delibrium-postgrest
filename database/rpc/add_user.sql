create or replace function aula.add_user (
    first_name text,
    last_name text,
    username text,
    user_role bigint,
    email text default null,
    idea_space bigint default null,
    school bigint default null
  ) returns void language plpython3u
as $$
import json
import random
import string

plpy.info('USERNAME', username, school)

# Get school id from JWT
res_school_id = plpy.execute(
    "select current_setting('request.jwt.claim.school_id');"
)
if len(res_school_id) == 0:
    plpy.error('Current user is not associated with a school.', sqlstate='PT401')
admin_school_id = res_school_id[0]['current_setting']

# Get User id
res_calling_user_id = plpy.execute(
    "select current_setting('request.jwt.claim.user_id');"
  )
if len(res_calling_user_id) == 0:
    plpy.error('Did not find user associated with this request.', sqlstate='PT401')
calling_user_id = res_calling_user_id[0]['current_setting']

# Check if user is admin
is_admin_plan = plpy.prepare(
    "select aula.is_admin($1);", ["bigint"]
  )
is_admin = plpy.execute(is_admin_plan, [school])

# Check if user is school_admin
is_school_admin_plan = plpy.prepare(
    "select aula.is_school_admin($1);", ["bigint"]
)
is_school_admin = plpy.execute(is_school_admin_plan, [school])

# Check if user has permission to create user
check_create_user_permission = plpy.prepare("select aula.check_permission('create_user', $1, null, $2)", ["bigint", "bigint"])
has_create_user_permission = plpy.execute(check_create_user_permission, [calling_user_id, school])

if is_admin[0]['is_admin'] or is_school_admin[0]['is_school_admin'] or has_create_user_permission[0]['check_permission']:
    plpy.execute('set "request.jwt.claim.is_admin" TO "True"')
else:
  plpy.error('Insufficient rights to create a user.')

if not school:
  school_id = admin_school_id
else:
  school_id = school

def create_random_password(with_dict = False):
    def random_without_dict(size = 6, with_upper = False):
        chars = string.ascii_lowercase + string.digits
        if with_upper:
          chars += string.ascii_uppercase
        return  ''.join(random.choice(chars) for _ in range(size))

    if with_dict:
      try:
          dict_location = '/usr/share/dict/words'
          with open(dict_location) as f:
              words = f.readlines()
          return ".".join([random.choice(words).strip() for _ in range(2)])
      except FileNotFoundError:
          plpy.warning("""Place a dictionary file in {} to enable
              word-based temp passwords""".format(dict_location))
          return random_without_dict()
    else:
      return random_without_dict()

password = create_random_password()
new_config = json.dumps({ "temp_password": password })

q1plan = plpy.prepare("""insert
    into aula_secure.user_login (school_id, password, config )
    values ($1, $2, $3) returning id ;""", ["bigint", "text", "jsonb"])
q1 = plpy.execute(q1plan, [school_id, password, new_config])
user_login = q1[0]

q2plan = plpy.prepare("""insert
    into aula.users (
        school_id,
        created_by,
        changed_by,
        user_login_id,
        first_name,
        last_name,
        email,
        username
    ) values ( $1, $2, $3, $4, $5, $6, $7, $8) returning id;
    """, ["bigint", "bigint", "bigint", "bigint", "text", "text", "text", "text"])
q2 = plpy.execute(q2plan, [ school_id,
                            calling_user_id,
                            calling_user_id,
                            user_login['id'],
                            first_name,
                            last_name,
                            email or '',
                            username])
user = q2[0]

q3plan = plpy.prepare("""insert
    into aula.user_role (school_id, created_by, changed_by, user_id, role_id, idea_space)
    values ( $1, $2, $3, $4, $5, $6);""", ["bigint", "bigint", "bigint", "bigint", "bigint", "bigint"])
q3 = plpy.execute(q3plan, [ school_id,
                            calling_user_id,
                            calling_user_id,
                            user['id'],
                            user_role,
                            idea_space or None])


q4plan = plpy.prepare("update aula_secure.user_login set aula_user_id= $1 where id= $2;", ["bigint", "bigint"])
q4 = plpy.execute(q4plan, [user['id'], user_login['id']])
$$;

grant execute on function aula.add_user (text, text, text, bigint, text, bigint, bigint) to aula_authenticator;