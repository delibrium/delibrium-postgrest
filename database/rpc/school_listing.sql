create or replace function aula.school_listing()
  returns json
  language plpython3u
as $$
    import json

    plpy.execute('set "request.jwt.claim.is_admin" TO "True"')

    result = plpy.execute("select id, name from aula.school order by name;")
    rv = [{
        "text": elem['name'],
        "value": elem['id']
    } for elem in result]

    return json.dumps(rv)
$$;
