create or replace function aula.ideas_space_user(spaceid bigint default null, schoolid bigint default null)
    returns json
    language plpython3u
as $$

import json

if schoolid is None:
  res_school_id = plpy.execute("select current_setting('request.jwt.claim.school_id');")
  if len(res_school_id) == 0:
      plpy.error('Current user is not associated with a school.', sqlstate='PT401')
  school_id = res_school_id[0]['current_setting']
else:
  school_id = schoolid

if spaceid:
  direct_space_members_plan = plpy.prepare("""
      select 
        distinct(user_id), us.username
        from aula.user_role as ur
        join aula.users as us 
          on us.id = ur.user_id
        where 
          ur.school_id = $1
          and ur.idea_space = $2
          and us.config->'deleted' is null
      union
      select 
        distinct(user_id), us.username
        from aula.user_role as ur 
        join aula.roles as r 
          on ur.role_id = r.id 
        join aula.users as us 
          on us.id = ur.user_id 
        join jsonb_each_text(r.permissions) p 
          on true 
        where 
          value = '["all"]'
          and ur.school_id = $1 
          and us.config->'deleted' is null
  """, ['bigint', 'bigint'])
  users = plpy.execute(direct_space_members_plan, [school_id, spaceid])
  return json.dumps([user for user in users])
$$;

grant execute on function aula.ideas_space_user(bigint, bigint) to aula_authenticator;
