create or replace function aula.remove_user_roles_bulk(schoolid bigint, userids bigint[], roleid bigint, ideaspace bigint)
  returns boolean
  language plpython3u
  set search_path = public, aula
  as $$

user_id_list = ",".join(map(str, userids))
remove_string = "DELETE FROM aula.user_role WHERE "

if ideaspace:
  remove_string += "school_id={} AND idea_space={} AND role_id='{}' AND user_id IN ({})".format(schoolid, ideaspace, roleid, user_id_list)
else:
  remove_string += "school_id={} AND idea_space is null AND role_id='{}' AND user_id IN ({})".format(schoolid, roleid, user_id_list)

plpy.execute(remove_string)

return True
$$;
