create or replace function aula.user_listing(schoolid bigint default null, spaceid text default null)
    returns json
    language plpython3u
as $$
    import json

    if not schoolid:
      res_school_id = plpy.execute("select current_setting('request.jwt.claim.school_id');")
      if len(res_school_id) == 0:
          plpy.error('Current user is not associated with a school.', sqlstate='PT401')

      school_id = res_school_id[0]['current_setting']
    else:
      school_id = schoolid

    res_calling_user_id = plpy.execute("select current_setting('request.jwt.claim.user_id');")
    if len(res_calling_user_id) == 0:
        plpy.error('Did not find user associated with this request.', sqlstate='PT401')
    calling_user_id = res_calling_user_id[0]['current_setting']

    is_admin = plpy.execute("select current_setting('request.jwt.claim.is_admin');")
    is_school_admin = plpy.execute("select current_setting('request.jwt.claim.is_school_admin');")

    if is_admin[0]['current_setting'] == 'False':
        check_create_permission = plpy.prepare("select aula.check_permission('create_user', $1, null, $2)", ["bigint", "bigint"])
        create_permission = plpy.execute(check_create_permission, [calling_user_id, school_id])

        check_delete_permission = plpy.prepare("select aula.check_permission('delete_user', $1, null, $2)", ["bigint", "bigint"])
        delete_permission = plpy.execute(check_delete_permission, [calling_user_id, school_id])

        check_edit_permission = plpy.prepare("select aula.check_permission('edit_user', $1, null, $2)", ["bigint", "bigint"])
        edit_permission = plpy.execute(check_edit_permission, [calling_user_id, school_id])

        if create_permission[0]['check_permission'] is True or delete_permission[0]['check_permission'] or edit_permission[0]['check_permission']:
            plpy.execute('set "request.jwt.claim.is_admin" TO "True"')

    if is_school_admin[0]['current_setting'] == 'True':
        plpy.execute('set "request.jwt.claim.is_admin" TO "True"')

    rv = plpy.execute("""
        select
        us.id,
	    us.first_name,
	    us.last_name,
	    us.username,
        us.email,
        us.is_admin,
        us.is_school_admin,
        ul.config,
        array_agg(row(
                rl.name,
                ur.idea_space,
                sp.title
            )) filter (where num_nulls(rl.name) = 0) as roles
        from
            aula.users as us
            join
                aula_secure.user_login as ul
                on ul.id=us.user_login_id
            left join
                aula.user_role as ur
                on ur.user_id = us.id
            left join
                aula.roles as rl
                on rl.id = ur.role_id
            left join
                aula.idea_space as sp
                on ur.idea_space = sp.id
        where us.school_id={}
        group by (us.id, us.username, ul.config);
    """.format(school_id))

    return json.dumps([user for user in rv])
$$;

grant execute on function aula.user_listing(bigint, text) to aula_authenticator;
