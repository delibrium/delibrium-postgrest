alter table aula.page enable row level security;

-- SELECT
drop policy if exists page_select on aula.page;
create policy page_select
  on aula.page
  for SELECT
  using 
  (
    aula.is_admin(school_id) or
    aula.from_school(school_id)
  );

-- INSERT
drop policy if exists page_insert on aula.page;
create policy page_insert
  on aula.page
  for INSERT
  with check 
  ( 
    created_by = request.user_id() and 
    (
      aula.is_admin(school_id) or
      aula.is_school_admin(school_id) or
      (
        aula.check_permission('create_page', request.user_id(), null, school_id)
      )
    )
  );

-- UPDATE
drop policy if exists page_update on aula.page;
create policy page_update
  on aula.page
  for UPDATE
  using 
  (
    aula.is_admin(school_id) or 
    aula.is_school_admin(school_id) or
    aula.check_permission('edit_page', request.user_id(), null, school_id)
  );

-- DELETE
drop policy if exists page_delete on aula.page;
create policy page_delete
  on aula.page
  for DELETE
  using 
  (
    aula.is_admin(school_id) or 
    aula.is_school_admin(school_id) or
    aula.check_permission('delete_page', request.user_id(), null, school_id)
  );