alter table aula.comment_vote enable row level security;

-- SELECT
drop policy if exists comment_vote_select on aula.comment_vote;
create policy comment_vote_select
  on aula.comment_vote 
  for SELECT 
  using 
  (
    aula.is_admin(school_id) or 
    aula.from_school(school_id)
  );

-- INSERT
drop policy if exists comment_vote_insert on aula.comment_vote;
create policy comment_vote_insert 
  on aula.comment_vote 
  for INSERT 
  with check 
  (
    created_by = request.user_id() and
    (
      aula.is_admin(school_id) or
      aula.is_school_admin(school_id) or 
      (
        aula.from_school(school_id) and 
        aula.check_permission('support_comment', request.user_id(), (select idea_space from aula.comment as c join aula.idea as i on c.parent_idea = i.id where c.id = comment), school_id)
      )
    )
  );

-- UPDATE
drop policy if exists comment_vote_update on aula.comment_vote;
create policy comment_vote_update
  on aula.comment_vote 
  for UPDATE 
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or 
    (
      aula.from_school(school_id) and 
      aula.is_owner(created_by) and
      aula.check_permission('support_comment', request.user_id(), (select idea_space from aula.comment as c join aula.idea as i on c.parent_idea = i.id where c.id = comment), school_id)
    )
  ) 
  with check 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or 
    (
      aula.from_school(school_id) and 
      aula.is_owner(created_by) and
      aula.check_permission('support_comment', request.user_id(), (select idea_space from aula.comment as c join aula.idea as i on c.parent_idea = i.id where c.id = comment), school_id)
    )
  );

-- DELETE
drop policy if exists comment_vote_delete on aula.comment_vote;
create policy comment_vote_delete
  on aula.comment_vote
  for DELETE 
  using 
  (
    aula.is_admin(school_id) or 
    aula.is_school_admin(school_id) or
    (
      aula.from_school(school_id) and 
      aula.is_owner(created_by) and
      aula.check_permission('support_comment', request.user_id(), (select idea_space from aula.comment as c join aula.idea as i on c.parent_idea = i.id where c.id = comment), school_id)
    )
  ); 
