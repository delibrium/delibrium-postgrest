alter table aula.user_role enable row level security;

drop policy if exists user_role_select on aula.user_role;
create policy user_role_select
  on aula.user_role
  for SELECT
  using 
  (
    aula.is_admin(school_id) or
    aula.from_school(school_id)
  );

drop policy if exists user_role_insert on aula.user_role;
create policy user_role_insert
  on aula.user_role
  for INSERT
  with check 
  (
    created_by = request.user_id() and 
    (
      aula.is_admin(school_id) or
      aula.is_school_admin(school_id) or
      aula.check_permission('edit_user', request.user_id(), null, school_id) or
      aula.check_permission('create_user', request.user_id(), null, school_id) 
    )
  );

drop policy if exists user_role_update on aula.user_role;
create policy user_role_update
  on aula.user_role
  for UPDATE
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.check_permission('edit_user', request.user_id(), null, school_id)
  );

drop policy if exists user_role_delete on aula.user_role;
create policy user_role_delete
  on aula.user_role
  for DELETE
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.check_permission('delete_user', request.user_id(), null, school_id)
  );
