drop trigger if exists update_role_change_at on aula.roles;
create trigger update_role_change_at before update on aula.roles for each row execute procedure aula.update_changed_column();
