alter table aula.roles enable row level security;

-- SELECT
drop policy if exists roles_select on aula.roles;
create policy roles_select
  on aula.roles
  for SELECT
  using 
  (
    aula.is_admin(school_id) or
    aula.from_school(school_id)
  );

-- INSERT
drop policy if exists roles_insert on aula.roles;
create policy roles_insert
  on aula.roles
  for INSERT
  with check 
  (
    aula.is_admin(school_id) or
    -- allowing admins to use deviating created_by
    created_by = request.user_id() and 
    (
      aula.is_school_admin(school_id) or
      aula.check_permission('create_role', request.user_id(), null, school_id)
    )
  );

-- UPDATE
drop policy if exists roles_update on aula.roles;
create policy roles_update
  on aula.roles
  for UPDATE
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.check_permission('edit_role', request.user_id(), null, school_id)
  );

-- DELETE
drop policy if exists roles_delete on aula.roles;
create policy roles_delete
  on aula.roles
  for DELETE
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.check_permission('delete_role', request.user_id(), null, school_id)
  );