alter table aula.school enable row level security;

-- SELECT
drop policy if exists school_select on aula.school;
create policy school_select
  on aula.school
  for SELECT
  using 
  (
    aula.is_admin(id) or
    aula.from_school(id)
  );

-- INSERT
drop policy if exists school_insert on aula.school;
create policy school_insert
  on aula.school
  for INSERT
  with check 
  (
    aula.is_admin(id)
  );

-- UPDATE
drop policy if exists school_update on aula.school;
create policy school_update
  on aula.school
  for UPDATE
  using 
  (
    aula.is_admin(id) or
    aula.is_school_admin(id)
  );

-- DELETE
drop policy if exists school_delete on aula.school;
create policy school_delete
  on aula.school
  for DELETE
  using 
  (
    aula.is_admin(id)
  );