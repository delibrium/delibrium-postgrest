create table if not exists aula_secure.user_login (
    id                 bigserial     primary key,
    school_id          bigint        references aula.school (id),
    created_at         timestamptz   not null default now(),
    changed_at         timestamptz   not null default now(),
    session_count      int           default 0,
    password           text          not null,
    config             jsonb         default '{}',
    last_login         timestamptz
);
