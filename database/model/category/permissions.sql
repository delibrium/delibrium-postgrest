alter table aula.category enable row level security;

-- SELECT
drop policy if exists category_select on aula.category;
create policy category_select
  on aula.category
  for SELECT
  using 
  (
    aula.is_admin(school_id) or
    aula.from_school(school_id)
  );

-- INSERT
drop policy if exists category_insert on aula.category;
create policy category_insert
  on aula.category
  for INSERT
  with check 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.check_permission('create_category', request.user_id(), null, school_id)
  );

-- UPDATE
drop policy if exists category_update on aula.category;
create policy category_update
  on aula.category
  for UPDATE
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or
    aula.check_permission('edit_category', request.user_id(), null, school_id)
  );

-- DELETE
drop policy if exists category_delete on aula.category;
create policy category_delete
  on aula.category
  for DELETE
  using 
  (
    aula.is_admin(school_id) or 
    aula.is_school_admin(school_id) or
    aula.check_permission('delete_category', request.user_id(), null, school_id)
  ); 
