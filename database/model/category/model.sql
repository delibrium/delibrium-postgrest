create table if not exists aula.category (
    id          bigserial   primary key,
    school_id   bigint      references aula.school (id),
    changed_at  timestamptz not null default now(),
    name        text        not null,
    description text,
    image       text,
    position    int,
    def         boolean     default 'f'
);
