alter table aula.idea_like enable row level security;

-- SELECT
drop policy if exists idea_like_select on aula.idea_like;
create policy idea_like_select
  on aula.idea_like 
  for SELECT 
  using 
  (
    aula.is_admin(school_id) or 
    aula.from_school(school_id)
  );

-- INSERT
drop policy if exists idea_like_insert on aula.idea_like;
create policy idea_like_insert 
  on aula.idea_like 
  for INSERT 
  with check 
  (
    created_by = request.user_id() and
    (
      aula.is_admin(school_id) or
      aula.is_school_admin(school_id) or 
      (
        aula.from_school(school_id) and 
        aula.check_permission('support_idea', request.user_id(), (select idea_space from aula.idea where id = idea), school_id)
      )
    )
  );

-- UPDATE
drop policy if exists idea_like_update on aula.idea_like;
create policy idea_like_update
  on aula.idea_like 
  for UPDATE 
  using 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or 
    (
      aula.from_school(school_id) and 
      aula.is_owner(created_by) and
      aula.check_permission('support_idea', request.user_id(), (select idea_space from aula.idea where id = idea), school_id)
    )
  ) 
  with check 
  (
    aula.is_admin(school_id) or
    aula.is_school_admin(school_id) or 
    (
      aula.from_school(school_id) and 
      aula.is_owner(created_by) and
      aula.check_permission('support_idea', request.user_id(), (select idea_space from aula.idea where id = idea), school_id)
    )
  );

-- DELETE
drop policy if exists idea_like_delete on aula.idea_like;
create policy idea_like_delete
  on aula.idea_like
  for DELETE 
  using 
  (
    aula.is_admin(school_id) or 
    aula.is_school_admin(school_id) or
    (
      aula.from_school(school_id) and 
      aula.is_owner(created_by) and
      aula.check_permission('support_idea', request.user_id(), (select idea_space from aula.idea where id = idea), school_id)
    )
  ); 
