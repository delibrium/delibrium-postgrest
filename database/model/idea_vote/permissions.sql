alter table aula.idea_vote enable row level security;

-- SELECT
drop policy if exists idea_vote_select on aula.idea_vote;
create policy idea_vote_select
  on aula.idea_vote 
  for SELECT 
  using 
  (
    aula.is_admin(school_id) or 
    aula.from_school(school_id)
  );

-- INSERT
drop policy if exists idea_vote_insert on aula.idea_vote;
create policy idea_vote_insert 
  on aula.idea_vote 
  for INSERT 
  with check 
  (
    created_by = request.user_id() and
    (
      aula.is_admin(school_id) or
      aula.is_school_admin(school_id) or
      delegation.is_delegated(user_id, request.user_id(), idea) or
      (
        aula.from_school(school_id) and 
        aula.check_permission('vote_idea', request.user_id(), (select idea_space from aula.idea where id = idea), school_id)
      )
    )
  );

-- UPDATE
drop policy if exists idea_vote_update on aula.idea_vote;
create policy idea_vote_update
  on aula.idea_vote 
  for UPDATE 
  using 
  (
    user_id = request.user_id() and 
    (
      aula.is_admin(school_id) or
      aula.is_school_admin(school_id) or
      delegation.is_delegated(user_id, request.user_id(), idea) or 
      (
        aula.from_school(school_id) and 
        aula.is_owner(created_by) and
        aula.check_permission('vote_idea', request.user_id(), (select idea_space from aula.idea where id = idea), school_id)
      )
    )
  ) 
  with check 
  (
    user_id = request.user_id() and 
    (
      aula.is_admin(school_id) or
      aula.is_school_admin(school_id) or
      delegation.is_delegated(user_id, request.user_id(), idea) or 
      (
        aula.from_school(school_id) and 
        aula.is_owner(created_by) and
        aula.check_permission('vote_idea', request.user_id(), (select idea_space from aula.idea where id = idea), school_id)
      )
    )
  );

-- DELETE
drop policy if exists idea_vote_delete on aula.idea_vote;
create policy idea_vote_delete
  on aula.idea_vote
  for DELETE 
  using 
  (
    aula.is_admin(school_id) or 
    aula.is_school_admin(school_id) or
    delegation.is_delegated(user_id, request.user_id(), idea) or
    (
      aula.from_school(school_id) and 
      aula.is_owner(created_by) and
      aula.check_permission('vote_idea', request.user_id(), (select idea_space from aula.idea where id = idea), school_id)
    )
  ); 
