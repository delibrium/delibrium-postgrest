DO language plpython3u $$
    import subprocess
    import io

    ###
    ## Set variables
    ###
    
    base_dir = '/docker-entrypoint-initdb.d/'
    check_usernames_path = base_dir + 'rpc/check_usernames.sql'
    user_id_path = base_dir + 'authorization/user_id.sql'
    login_path = base_dir + 'authorization/login.sql'
    add_school_path = base_dir + 'rpc/add_school.sql'
    add_user_path = base_dir + 'rpc/add_user.sql'
    user_permissions_path = base_dir + 'model/user/permissions.sql'

    # Helper function for printing output
    def monitor_process(process):
        for line in io.TextIOWrapper(process.stdout, encoding="utf-8"):
            plpy.info(line.rstrip())
        errcode = process.wait()
        return errcode

    ###
    ## Insert check usernames rpc function
    ###

    plpy.info('--- Insert check usernames rpc function ---')
   
    insert_check_usernames_rpc = subprocess.Popen(['psql', 'app', 'superuser', '-f', check_usernames_path], stdout = subprocess.PIPE)
    insert_check_usernames_rpc_exit_code = monitor_process(insert_check_usernames_rpc)

    if insert_check_usernames_rpc_exit_code != 0:
        plpy.error('--- Error while inserting check usernames rpc function ---')
    
    plpy.info('--- Finished inserting check usernames rpc function ---')
    
    ###
    ## Drop unused column 'login' in aula_secure.user_login
    ###

    plpy.info('--- Drop unused column login ---')

    drop_login_column = plpy.execute("ALTER TABLE aula_secure.user_login DROP COLUMN IF EXISTS login")

    plpy.info('--- Finished dropping unused column login ---')

    ###
    ## Add old_username column to aula.users
    ###

    plpy.info('--- Add old_username column ---')

    add_old_username_column = plpy.execute("ALTER TABLE aula.users ADD COLUMN IF NOT EXISTS old_username text")

    plpy.info('--- Finished old_username column ---')
    
    ###
    ## Update duplicated usernames
    ###

    plpy.info('--- Update duplicated usernames ---')

    duplicated_usernames = plpy.execute("SELECT username FROM aula.users GROUP BY username HAVING COUNT(*) > 1")

    for username in duplicated_usernames:
        duplicate_ids_plan = plpy.prepare("SELECT id from aula.users WHERE username = $1 ORDER BY id", ["text"])
        duplicate_ids = plpy.execute(duplicate_ids_plan, [username['username']])
        
        index = 0
        for user_id in duplicate_ids:
            if index > 0:
                current_username = username['username']
                new_username = username['username'] + str(index)

                is_username_available = False
                
                while is_username_available != True:
                    check_username_available_plan = plpy.prepare("SELECT count(id) FROM aula.users where username = $1", ["text"])
                    check_username_available = plpy.execute(check_username_available_plan, [new_username])
                    
                    if check_username_available[0]['count'] > 0:
                        index += 1
                        new_username = username['username'] + str(index)
                    else:
                        is_username_available = True
                
                update_username_plan = plpy.prepare("UPDATE aula.users SET username = $1 WHERE id = $2", ["text", "bigint"])
                update_username = plpy.execute(update_username_plan, [new_username, user_id['id']])

                set_old_username_plan = plpy.prepare("UPDATE aula.users SET old_username = $1 WHERE id = $2", ["text", "bigint"])
                set_old_username = plpy.execute(set_old_username_plan, [current_username, user_id['id']])
                
            index += 1
    plpy.info('--- Finished updating duplicated usernames ---')

    ###
    ## Adjust tables and functions
    ###

    plpy.info('--- Adjust tables and functions ---')

    """duplicated_usernames = plpy.execute("ALTER TABLE aula.users ADD CONSTRAINT unique_username unique(username)")"""
    drop_login = plpy.execute("DROP FUNCTION IF EXISTS aula.login(bigint, text, text)")
    drop_user_id = plpy.execute("DROP FUNCTION IF EXISTS aula_secure.user_id(bigint, text, text)")

    plpy.info('--- Finished adjusting tables and functions ---')

    ###
    ## Update user_id function
    ###

    plpy.info('--- Update user_id function ---')
   
    update_user_id = subprocess.Popen(['psql', 'app', 'superuser', '-f', user_id_path], stdout = subprocess.PIPE)
    update_user_id_exit_code = monitor_process(update_user_id)

    if update_user_id_exit_code != 0:
        plpy.error('--- Error while updating user id function ---')
    
    plpy.info('--- Finished updating check user id function ---')

    ###
    ## Update login function
    ###

    plpy.info('--- Update login function ---')
   
    update_login = subprocess.Popen(['psql', 'app', 'superuser', '-f', login_path], stdout = subprocess.PIPE)
    update_login_exit_code = monitor_process(update_login)

    if update_login_exit_code != 0:
        plpy.error('--- Error while updating login function ---')
    
    plpy.info('--- Finished updating login function ---')

    ###
    ## Update add_school function
    ###

    plpy.info('--- Update add_school function ---')
   
    update_add_school = subprocess.Popen(['psql', 'app', 'superuser', '-f', add_school_path], stdout = subprocess.PIPE)
    update_add_school_exit_code = monitor_process(update_add_school)

    if update_add_school_exit_code != 0:
        plpy.error('--- Error while updating add_school function ---')
    
    plpy.info('--- Finished updating add_school function ---')

    ###
    ## Update add_user function
    ###

    plpy.info('--- Update add_user function ---')
   
    update_add_user = subprocess.Popen(['psql', 'app', 'superuser', '-f', add_user_path], stdout = subprocess.PIPE)
    update_add_user_exit_code = monitor_process(update_add_user)

    if update_add_user_exit_code != 0:
        plpy.error('--- Error while updating add_user function ---')
    
    plpy.info('--- Finished updating add_user function ---')

    ###
    ## Update user table permissions
    ###

    plpy.commit()
    plpy.info('--- Update user table permissions ---')
   
    update_user_permissions = subprocess.Popen(['psql', 'app', 'superuser', '-f', user_permissions_path], stdout = subprocess.PIPE)
    update_user_permissions_exit_code = monitor_process(update_user_permissions)

    if update_user_permissions_exit_code != 0:
        plpy.error('--- Error while updating user table permissions ---')
    
    plpy.info('--- Finished updating user table permissions ---')
$$;
